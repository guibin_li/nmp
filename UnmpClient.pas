(************************************************************************
*                                                                       *
*   UnmpClient.pas -- This module Create NamedPipe is Client.           *
*                                                                       *
*   Copyright 2001 - 2007 CationSoft Corp. All rights reserved.         *
*                                                                       *
************************************************************************)
//2006.12.22
//xiaobin


unit UnmpClient;

interface
  uses Windows;

var
  m_hInPipe:THandle;


function CreateNamedPipeClient:DWORD;stdcall;

implementation

function CreateNamedPipeClient:DWORD;
var
  connPipe:Boolean;
begin
  connPipe:=  WaitNamedPipe('\\.\pipe\xiaobin',120000);
  if connPipe=false then
  begin
    Result:=0;
  end;

  m_hInPipe:=  CreateFile('\\.\pipe\xiaobin',
        GENERIC_READ,
        0,
        nil,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        0);
  if (m_hInPipe=0) or (m_hInPipe=INVALID_HANDLE_VALUE) then
  begin
    //CloseHandle(m_hInPipe);
    Result:=0;
  end;

  Result:=m_hInPipe;
end;

end.
