(************************************************************************
*                                                                       *
*   UnmpClient.pas -- This module Create NamedPipe is Server.           *
*                                                                       *
*   Copyright 2001 - 2007 CationSoft Corp. All rights reserved.         *
*                                                                       *
************************************************************************)
//2006.12.22
//xiaobin


unit UnmpSrv;

interface
  uses Windows;

const
  PIPE_BUF_SIZE=254;
  PIPE_TIMEOUT=120000;

var
  m_hOutPipe:THandle;


function CreateNamedPipeServer:DWORD;stdcall;

implementation

function CreateNamedPipeServer:DWORD;
begin
  //create server->client
  m_hOutPipe:=CreateNamedPipe('\\.\pipe\xiaobin'
                        ,PIPE_ACCESS_OUTBOUND
                        ,PIPE_TYPE_MESSAGE
                        ,5
                        ,PIPE_BUF_SIZE
                        ,0
                        ,PIPE_TIMEOUT
                        ,nil);
                        
  if ((m_hOutPipe =0)or(m_hOutPipe = INVALID_HANDLE_VALUE))then
  begin
    CloseHandle(m_hOutPipe);
    Result:=0;
  end;

  if ConnectNamedPipe(m_hOutPipe,nil)=true then
  begin
    if GetLastError=ERROR_PIPE_NOT_CONNECTED then
    begin
      Result:=0;
    end;
  end
  else
  begin
    CloseHandle(m_hOutPipe);
    Result:=0;
  end;
  
  Result:=m_hOutPipe;
end;

end.
