library npSupport;

uses
  UnmpSrv in 'UnmpSrv.pas',
  UnmpClient in 'UnmpClient.pas';

{$R *.res}

exports
  CreateNamedPipeServer name 'createNMP',
  CreateNamedPipeClient name 'connectNMP';

begin
end.
 